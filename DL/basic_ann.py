import tensorflow as tf
import numpy as np
n_inputs=3
n_outputs=2

x=tf.placeholder(shape=[None,n_inputs], dtype=tf.float32)
#y=tf.placeholder(shape=[n_outputs],dtype=tf.float32)

var_1_2=tf.Variable(tf.random_normal(shape=[n_inputs,n_outputs]),dtype=tf.float32)

init=tf.global_variables_initializer()

l_1_2=tf.matmul(x,var_1_2)

x0_batch = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 0, 1]])

with tf.Session() as sess:
	init.run()
	for i in range (4):
		a=sess.run(var_1_2,feed_dict={x:x0_batch})
		print(a)
		
