import tensorflow as tf

#importing data from excel file to python list
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot = True)



batch_size = 100
total_instances=mnist.train.num_examples
#defining placeholders 
x = tf.placeholder('float', [None, 784])
y = tf.placeholder('float', [None, 784])



#computational graph
#2 hidden layers with 12 9 neurons
def neural_network(data):

    #set dimention of weights and biases in python
    #quotations on weights, biases can be avoided they are just used to denote them as variables
    #[rows,columns]
    par_1_2={'weights':tf.Variable(tf.random_normal([784,12])),
                                     'biases':tf.Variable(tf.random_normal([12]))}
    par_2_3={'weights':tf.Variable(tf.random_normal([12,9])),
                                     'biases':tf.Variable(tf.random_normal([9]))}
    par_3_4={'weights':tf.Variable(tf.random_normal([9,784])),
                                     'biases':tf.Variable(tf.random_normal([784]))}
                                                          
    #building network
    #x has to be a matrix else tf.matmul will give error
    #dimention matching has to be taken care of in matmul                                                         
    layer_1_2=tf.add(tf.matmul(data,par_1_2['weights']),par_1_2['biases'])  
    layer_1_2=tf.nn.relu(layer_1_2)                                               
    layer_2_3=tf.add(tf.matmul(layer_1_2,par_2_3['weights']),par_2_3['biases'])     
    layer_2_3=tf.nn.relu(layer_2_3) 
    output=tf.add(tf.matmul(layer_2_3,par_3_4['weights']),par_3_4['biases'])        
    
    #softmax can be used here or later/doesnt matter
    #tf.nn.softmax()
    
    return [output, layer_2_3]






#computation
def train(x,y):
    
    prediction,temp=neural_network(x)

  #logits and labels must have the dimension[batch_size,classes]
    #tf.reduce mean will take mean across all the dimentions and will give a single value 
    cost=tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction,labels=y))
    optimizer=tf.train.AdamOptimizer().minimize(cost)
    total_epochs=10
    #start the session this will start computation and will give the output in 1 go
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())     
        for epoch in range(total_epochs):
            epoch_loss=0
            for i in range (int(total_instances/batch_size)):    
                #epoch_x,epoch_y
                epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                _,c,features=sess.run([optimizer,cost,temp],feed_dict={x:epoch_x,y:epoch_x})                                        
                epoch_loss +=c
            
            print("Epoch loss: ",epoch_loss)
        
        print(features)
        correct=tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
        accuracy=tf.reduce_mean(tf.cast(correct,'float'))
        print('Accuracy:',accuracy.eval({x:mnist.test.images, y:mnist.test.images}))
                                                          
          



                                                          
#call the function
train(x,y)                                                        


