#None is used when first dimension is variable
x= tf.placeholder(tf.float32, shape=[None, 2])

#input to placeholder cannot be tensor
a1=[[2,3],[4,5]]
matrix_multiplication=tf.matmul(x,x)
sess.run(matrix_multiplication,feed_dict={x:a1})
